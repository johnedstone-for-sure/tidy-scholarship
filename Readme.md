## Some examples

*Works on Windows, Linux, and no doubt Mac.  Currently this repo is public for documentation purposes*

Note: the path can be in a subdirectory, or in your current directory for the 
input and output.  The examples below use a subdirectory, but it is not necessary.

### Assumptions

- First row is a header that looks like this
`First Name	Last Name	Parish & Grade School	University	Award	Amount	Grants, etc.	Amount`

- Consists of rows, with no empty rows, with only the above information

### Here is how to use this script
```
venv/bin/python tidy2020_scholarship.py -p data/AcceptancesVersion1Dataxlsx.xlsx -o data/AcceptancesVersion1Dataxlsx.html
```

### Here is how to see the help message in case you forget
```
venv/bin/python tidy2020_scholarship.py -h

usage: tidy2020_scholarship.py [-h] -p PATH -o OUTPUT

Convert an Excel(.xlsx) file to an HTML table

optional arguments:
  -h, --help            show this help message and exit
  -p PATH, --path PATH  relative path to Excel file, e.g. data/myexcel.xlsx
  -o OUTPUT, --output OUTPUT
                        name of output file, e.g. output.html
```

### Here is what you get if you forget the arguments. Notice there is a reminder to use -h
```
venv/bin/python tidy2020_scholarship.py

usage: tidy2020_scholarship.py [-h] -p PATH -o OUTPUT
tidy2020_scholarship.py: error: the following arguments are required: -p/--path, -o/--output

```

### Here is an example, where the user gave the wrong path.  Notice that the error message gives you helpful information.
```
venv/bin/python tidy2020_scholarship.py -p data/AcceptancesVersion1Dataxlsx.xlsx -o dat/AcceptancesVersion1Dataxlsx.html
ERROR:root:
        There was a mishap somewhere!!!
        Oh my, contact someone!!!!!
        The Cat in the Hat is in the House!!!
        Here is the error:
        [Errno 2] No such file or directory: 'dat/AcceptancesVersion1Dataxlsx.html'
```
