# -*- coding: utf-8 -*-

import argparse
import arrow
from collections import OrderedDict
from datetime import datetime
import logging
from openpyxl import load_workbook
from pprint import pprint

DEBUG = False
if DEBUG:
    logging.basicConfig(level=logging.INFO, format="%(funcName)s():%(lineno)i: %(message)s %(levelname)s")
else:
    logging.basicConfig(level=logging.WARNING)

parser = argparse.ArgumentParser(description='Convert an Excel file to an HTML table')
parser.add_argument("-p", "--path", required=True, help="relative path to Excel file, e.g. data/myexcel.xlsx")
parser.add_argument("-o", "--output", required=True, help="name of output file, e.g. output.html")

def write_results(o, results):
    logging.info(results.keys())

    newline = '\r\n'
    sp = lambda x: x or '&nbsp;'
    zero = lambda x: x or 0
    total = 0
    utc = arrow.utcnow()
    local = utc.to('America/New_York').format('MMM DD, YYYY h:mm A ZZZ')

    with open(o, 'w') as fh:
        first_loop = True
        for id in results.keys():
            logging.info(id)
            fh.write("""<table class="acceptancetable"><tbody>{nl}""".format(nl=newline))
            if first_loop:
                fh.write("""    <tr><th colspan="2">Student Name - Parish/School - Acceptances - Scholarships, Grants, Awards</th></tr>{}""".format(newline))
                first_loop = False
            fh.write("""    <tr><th colspan="2">{}&nbsp;<span class="acceptanceparish">({})</span></th></tr>{}""".format(
                id, results[id]['parish'], newline))

            awards = results[id]['awards'].keys()
            logging.info(awards)

            if 'unknown' in awards:
                for ea in results[id]['awards']['unknown']:
                    what, amt = ea
                    if what or amt:
                        total += zero(amt)
                        logging.info(ea)
                        fh.write("""            <tr><td class="acceptanceaward">{}</td><td class="acceptancedollar">${:,}</td></tr>{}""".format(
                            sp(what), zero(amt), newline).replace('$0', '&nbsp;'))
                fh.write(newline)

            for univ in awards:
                if univ != 'unknown':
                    fh.write("""        <tr><td class="acceptancecollege" colspan="2" scope="rowgroup">{}</td></tr>{}""".format(univ, newline))
                    for ea in results[id]['awards'][univ]:
                        what, amt = ea
                        if what or amt:
                            total += zero(amt)
                            logging.info(ea)
                            fh.write("""            <tr><td class="acceptanceaward">{}</td><td class="acceptancedollar">${:,}</td></tr>{}""".format(
                            sp(what), zero(amt), newline).replace('$0', '&nbsp;'))
            fh.write("""</tbody></table>{nl}{nl}""".format(nl=newline))


        fh.write("""{nl}<hr>{nl}<table class="acceptancetable"><tbody>{nl}<tr><th class="acceptanceaward">Total</th><th>${total:,}</th></tr>{nl}<tr style="display:none"><td>Updated</td><td>{local}</td></tr>{nl}</tbody></table>{nl}""".format(total=total, local=local, nl=newline))
        logging.info(local)
        logging.info('{:,}'.format(total))


def read_excel(p):
    wb = load_workbook(filename=p)
    ws = wb[wb.sheetnames[0]]
    logging.info(p)

    # Get names
    od = OrderedDict()
    for row_cells in ws.iter_rows(min_row=2):
        logging.info(row_cells)
        logging.info(len(row_cells))
        #for cell in list(row_cells)[0:2]:
        #   print('%s: cell.value=%s' % (cell, cell.value) )

        fn, ln, parish, univ, award, amt, grant, dollar  = list(row_cells)[0:8]
        id = '{} {}'.format(fn.value, ln.value).strip()

        if univ.value:
            univ_str = univ.value
        else:
            univ_str = 'unknown'

        # First occurance of this person
        if not od.get(id):
            od[id] = {'parish': parish.value.replace('&', '&amp;'), 'awards': OrderedDict()}
            od[id]['awards'][univ_str] = [(award.value, amt.value), (grant.value, dollar.value)]
        else:
            # Does univ exist?
            if od[id]['awards'].get(univ_str):
                od[id]['awards'][univ_str].extend([(award.value, amt.value), (grant.value, dollar.value)])
            else:
                od[id]['awards'][univ_str] = [(award.value, amt.value), (grant.value, dollar.value)]



    logging.info(od)
    #pprint(od)
    students = od.keys()
    logging.info(students)
    logging.info((len(students)))

    return od

if __name__ == '__main__':

    try:
        args = parser.parse_args()
        p = args.path
        o = args.output

        results = read_excel(p)
        write_results(o, results)
    except Exception as e:
        logging.error("""
        There was a mishap somewhere!!!
        Oh my, contact someone!!!!!
        The Cat in the Hat is in the House!!!
        Here is the error:
        {}
        """.format(e))

# vim: ai et ts=4 sw=4 sts=4 nu ru
