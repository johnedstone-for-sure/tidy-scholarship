#!/usr/bin/env python
## Author: EJohnstone
## Date: 03-Dec-2008
import re
import os
import sys
import datetime

def tidy_csv(file_to_read):
    '''Purpose: convert scholarship csv to html
    
    Usage
      ./tidy_scholarship.py  file.csv >file.php

    Notes:
        Use Open Office to save your spreadsheet as a 'Text CSV'
        Do not include the first header row from your spreadsheet
        Probably best to use UTF-8 encoding, depending on your context
        Use the pipe symbol, '|', for the Field Delimiter
        Use Nothing for the Text Delimiter
        The name of the file, including the extension, does not matter
    '''
    fh = open(file_to_read, 'r')
    data = fh.readlines()
    fh.close()
    patt_empty = re.compile('^$')
    
    ## Build dictionary, key == lastname_firstname, value = list of lists
    name_dict = {}
    for line in data:
        line.strip()
        line_list = line.split('|')
        line_list = [ patt_empty.sub('&nbsp;', x.strip().strip('\n')) for x in line_list]
        full_name = '%s %s' % (line_list[1], line_list[0])
        if name_dict.has_key(full_name):
           name_dict[full_name].append(line_list) 
        else:
           name_dict[full_name] = [] 
           name_dict[full_name].append(line_list) 
    
    ## Print table
    ## print """
    ##   <style>
    ##     table { border-collapse: separate; }
    ##     .row { background-color: #ccc; }
    ##   </style>
    ## """

    d = datetime.date.today()
    date_written = d.strftime('%B %d, %Y')
    print """   
                  <td  id="centerContent" colspan="2">
                    <div class="Item">
                      <p class="title" > 
                        Class of 2013  College Information
                      </p>
                      <div style="float:right;padding-right:20px;">
                        <a href="/seniors">Return to Senior Page</a>
                      </div>
                      <div class="more"  style="padding-left:20px;padding-top:20px;">
                        --Update %(date_written)s
                      </div>
    """ % {'date_written': date_written}
    print """   
    <table class="tableAC" style="line-height:130%;text-align:left;margin-left:0px;font-size:11px;">
      <tr class="bold" style="font-size:12px;">
        <td>Student Name<br>
           Parish/Grade School</td>
        <td>Accepted to:</td>
        <td>Awarded Scholarship</td>
        <td>Amount</td>
      </tr>"""

    ## For those with one scholoarship, print 2 rows
    ## These three lines for python 2.3
    dict_key = name_dict.keys(); 
    dict_key.sort()
    for key in dict_key:
    ## for key in sorted(name_dict.iterkeys()):
        if len(name_dict[key]) ==  1:
            print """      <tr class="row2">
        <td>%(fn)s %(ln)s</td>
        <td>%(univ)s</td>
        <td>%(name)s</td>
        <td>%(amount)s</td>
      </tr>
      <tr>
        <td>%(parish)s</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>""" % {
                  'fn': name_dict[key][0][0],
                  'ln': name_dict[key][0][1],
                  'univ': name_dict[key][0][3],
                  'name': name_dict[key][0][4],
                  'amount': name_dict[key][0][5],
                  'parish': name_dict[key][0][2],
                  }
        ## For those with more than one scholarship
        else:
            ## Print first row
            print """     <tr class="row2">
        <td>%(fn)s %(ln)s</td>
        <td>%(univ)s</td>
        <td>%(name)s</td>
        <td>%(amount)s</td>
      </tr>""" % {
                  'fn': name_dict[key][0][0],
                  'ln': name_dict[key][0][1],
                  'univ': name_dict[key][0][3],
                  'name': name_dict[key][0][4],
                  'amount': name_dict[key][0][5],
                 }
            ## Print after the first row
            for i in name_dict[key][1:]:
                ## Print 2nd row
                if name_dict[key].index(i) == 1:
                    print """      <tr>
        <td>%(parish)s</td>
        <td>%(univ)s</td>
        <td>%(name)s</td>
        <td>%(amount)s</td>
      </tr>""" % {
                  'parish': i[2],
                  'univ': i[3],
                  'name': i[4].strip(),
                  'amount': i[5],
                }
                if name_dict[key].index(i) > 1:
                    print """      <tr>
        <td>&nbsp;</td>
        <td>%(univ)s</td>
        <td>%(name)s</td>
        <td>%(amount)s</td>
      </tr>""" % {
                  'univ': i[3],
                  'name': i[4],
                  'amount': i[5],
                 }
    print """    </table>
  </div>
    """

if __name__ == '__main__':
    options = ['-h', '--help',]
    try:
      ## asking for help
        if len(sys.argv) == 2 and sys.argv[1] in options:
            ## print 'boo', sys.argv[0]
            print tidy_csv.__doc__
        elif len(sys.argv) == 2:
            tidy_csv(sys.argv[1])
        else:
            print tidy_csv.__doc__
    except Exception, e:
        print e
        print tidy_csv.__doc__
        sys.exit(2)


# vim: ai et ts=4 sw=4 sts=4 nu
